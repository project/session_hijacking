<?php

namespace Drupal\session_hijacking\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class SessionHijacking.
 *
 * Provides functionality to protect against session hijacking by listening to
 * kernel request events and enforcing security measures such as IP address
 * consistency and browser session consistency.
 */
class SessionHijacking implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The session manager.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * The Logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a new SessionHijacking object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   The current user account.
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   *   The session manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $config_factory, AccountProxyInterface $account, SessionManagerInterface $session_manager, LoggerChannelFactoryInterface $loggerFactory, RequestStack $requestStack) {
    $this->connection = $connection;
    $this->configFactory = $config_factory;
    $this->currentUser = $account;
    $this->sessionManager = $session_manager;
    $this->loggerFactory = $loggerFactory;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['protectSessionHijacking', 31];
    return $events;
  }

  /**
   * Reads the IP address associated with the specified user's session.
   *
   * @param int $uid
   *   The user ID whose session IP address should be read.
   *
   * @return string
   *   The IP address associated with the user's session or an empty string if not found.
   */
  protected function read($uid) {
    // Ensure the user ID is not empty.
    if (empty($uid)) {
      return '';
    }

    // Query the sessions table to get the hostname (IP address) for the given user ID.
    $query = $this->connection->queryRange(
      'SELECT hostname FROM {sessions} WHERE uid = :uid',
      0,
      1,
      [':uid' => $uid]
    );

    // Fetch and return the hostname (IP address) as a string.
    return (string) $query->fetchField();
  }

  /**
   * Clears all cookies for the specified user.
   *
   * @param int $uid
   *   The user ID whose cookies should be cleared.
   */
  protected function clearCookies($uid) {
    // Ensure the user ID is not empty.
    if (!empty($uid)) {
      // Get all request cookies.
      $request_cookies = $this->requestStack->getCurrentRequest()->cookies->all();

      // Create a new response object.
      $response = new Response();

      // Iterate over each cookie and set it to expire immediately.
      foreach ($request_cookies as $cookie_name => $cookie_value) {
        // Create an expired cookie.
        $cookie = new Cookie($cookie_name, '', time() - 3600, '/', NULL, FALSE, TRUE);
        // Set the expired cookie in the response headers.
        $response->headers->setCookie($cookie);
      }
    }
  }

  /**
   * Protects against session hijacking by enforcing browser session consistency and IP address consistency.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to get the current request.
   */
  public function protectSessionHijacking(RequestEvent $event) {
    // Get the session hijacking configuration settings.
    $sessionHijackingConfig = $this->configFactory->get('session_hijacking.settings');
    $browser_session_hijacking = $sessionHijackingConfig->get('browser_session_hijacking') ?? 0;
    $ip_change_logout = $sessionHijackingConfig->get('ip_change_logout') ?? 0;

    // Get the current user.
    $user = $this->currentUser;

    // Proceed if the user is authenticated.
    if ($user->isAuthenticated()) {

      // Check if browser session hijacking protection is enabled.
      if ($browser_session_hijacking) {
        // Set the user agent in the session if not already set.
        if (!isset($_SESSION['HTTP_USER_AGENT'])) {
          $_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
        }

        // If the user agent has changed, log out the user.
        if ($_SESSION['HTTP_USER_AGENT'] !== $_SERVER['HTTP_USER_AGENT']) {
          unset($_SESSION['HTTP_USER_AGENT']);
          $this->clearCookies($user->id());
          $this->sessionManager->destroy();
          $user->setAccount(new AnonymousUserSession());

          // Log the event.
          $this->loggerFactory->get('session_hijacking')
            ->notice(
              $this->t('User forcibly logged out due to login attempt with old session detected in another browser console.')
            );

          // Redirect to the home page.
          $response = new RedirectResponse('/', 302);
          $response->send();
          return;
        }
      }

      // Check if IP change logout protection is enabled.
      if ($ip_change_logout) {
        // Get the current request and IP address.
        $request = $event->getRequest();
        $ip_address = $request->getClientIp();
        $session_ip = $this->read($user->id());

        // If the IP address has changed, log out the user.
        if ($ip_address !== $session_ip) {
          $this->clearCookies($user->id());
          $this->sessionManager->destroy();
          $user->setAccount(new AnonymousUserSession());

          // Log the event.
          $this->loggerFactory->get('session_hijacking')
            ->notice(
              $this->t('User forcibly logged out - Current IP: @ip, Session IP: @sess_ip', [
                '@ip' => $ip_address,
                '@sess_ip' => $session_ip
              ])
            );

          // Redirect to the home page.
          $response = new RedirectResponse('/', 302);
          $response->send();
          return;
        }
      }
    }
  }

}
