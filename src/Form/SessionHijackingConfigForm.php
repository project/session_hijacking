<?php

namespace Drupal\session_hijacking\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SessionHijackingConfigForm.
 *
 * Provides a configuration form for session hijacking settings.
 */
class SessionHijackingConfigForm extends ConfigFormBase {

  /**
   * Configuration object for session hijacking settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $sessionHijackingConfig;

  /**
   * The Config storage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new SessionHijacking object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory to retrieve session hijacking settings.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->sessionHijackingConfig = $config_factory->getEditable('session_hijacking.settings');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Get the form id.
   *
   * @return string
   *   Unique form id.
   */
  public function getFormId() {
    return 'session_hijacking_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'session_hijacking.settings',
    ];
  }

  /**
   * Generate the config Form Element.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState Object.
   *
   * @return array
   *   Return Form Element.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['session_hijacking_settings'] = [
      '#title' => $this->t('Session Hijacking Settings'),
      '#type' => 'fieldset',
    ];

    $form['session_hijacking_settings']['browser_session_hijacking'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable browser level session hijacking protect option.'),
      '#default_value' => $this->sessionHijackingConfig->get('browser_session_hijacking'),
    ];

    $form['session_hijacking_settings']['ip_change_logout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Save IP on login, Logging out User from one System if logged in another System.'),
      '#default_value' => $this->sessionHijackingConfig->get('ip_change_logout'),
    ];

    $form['session_hijacking_settings']['session_cookie_clear'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check this option to clear the session and cookies during user logout.'),
      '#default_value' => $this->sessionHijackingConfig->get('session_cookie_clear'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->sessionHijackingConfig
      ->set('browser_session_hijacking', $form_state->getValue('browser_session_hijacking'))
      ->set('ip_change_logout', $form_state->getValue('ip_change_logout'))
      ->set('session_cookie_clear', $form_state->getValue('session_cookie_clear'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
