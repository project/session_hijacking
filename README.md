# Session Hijacking

This module is utilized to safeguard against session hijacking at the browser level.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/session_hijacking).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/session_hijacking).


## Contents of this file

- Requirements
- Installation
- Configuration
- Troubleshooting
- Maintainers


## Requirements

No extra module is required.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

To enable session_hijacking:

1. Enable Session Hijacking module
2. Go to the admin interface (/admin/config/session-hijacking/settings).
3. Users can manage configuration using simple guidelines in the field's description.
4. Nothing else :)


## Troubleshooting

Session Hijacking module doesn't provide any visible functions to the user
on its own, it just provides security handling services.


## Maintainers

- Manikandan Ramakrishnan - [Manikandan Era](https://www.drupal.org/u/manikandan-era)
